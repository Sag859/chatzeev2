const express = require("express");
const app = express();
const server = require("http").Server(app);
const io = require("socket.io")(server);

io.on("connection", socket => {
  const { id } = socket.client;
  console.log(`User Connected: ${id}`);
  
  socket.on('chat message', data => {
  const { message, room, username } = data;
  console.log(`msg: ${message}, room: ${room}, user: ${username}, media: ${data.media}`);
  io.emit('chat message', 
    
    {
      username  : data.username,
      timestamp : data.timestamp,
      media     : data.media,
      fileName  : data.fileName,
      message   : data.message,
      room      : data.room
    });
  });

  socket.on('disconnect', () =>
  console.log(`Disconnected: ${socket.id}`));

  socket.on('join', (roomNo) => {
  console.log(`Socket ${id} joining ${roomNo}`);
  socket.join(roomNo);
  });
  
});

const PORT = process.env.PORT || 5000;
server.listen(PORT, () => console.log(`Listen on *: ${PORT}`));