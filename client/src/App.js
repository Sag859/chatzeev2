import React, {useState, useEffect, useCallback, useRef} from "react";
import io from "socket.io-client";
import Picker from 'emoji-picker-react'
import Dropzone from "./components/Dropzone";
import './App.css'

const socket = io.connect("http://localhost:5000");

function App() {
  
  const [msg, setMsg] = useState("");
  const [imgMsg, setImgMsg] = useState("");
  const [chat, setChat] = useState([]);
  const [username, setName] = useState("");
  const [roomNo, setRoom] = useState(1);
  const [chosenEmoji, setChosenEmoji] = useState(0);
  const [emojiStatus, setEmojiStatus] = useState(1)
  const [otherUser, setOtherUser] = useState("");
  const [fileStatus, setFileUpload] = useState(1)
  const usernameRef = useRef()
  const messagesEndRef = useRef(null)

  
  let appEmojiStatus = ''
  let fileSelStatus = ''

  if(fileStatus)
    fileSelStatus = 'appEmojiDisable'
  else
    fileSelStatus = 'appFilePick'

  if(emojiStatus)
    appEmojiStatus = 'appEmojiDisable'
  else
    appEmojiStatus = 'appEmojiPick'

  const onEmojiClick = (event, emojiObject) => {
    setChosenEmoji(emojiObject);
      
    if(chosenEmoji) {
      setMsg(msg+chosenEmoji.emoji)
      
    }
    
  };

  useEffect(()=>{
    if(roomNo){
      socket.emit('join',roomNo)
    }
    socket.on('chat message', data => {
      // Add new messages to existing messages in "chat"
      setChat(chat => [...chat, { username: data.username, message: data.message, media: data.media, timestamp: data.timestamp }]);
      
    });
    console.log('component mounted!')
    scrollToBottom();
  },[])

  const onTextChange = (e) =>{
    setMsg(e.target.value)  
  }

  const onImgChange = (e) =>{
    setImgMsg(e.target.value)
    var imgFile = e.target.files[0];
    onMessageImgSubmit(imgFile)
  }

  const onUserChange = (e) =>{
    setName(e.target.value)  
  }

  const onMessageSubmit = () =>{ 
    
    let data = { username: username, message: msg, room: roomNo}
    socket.emit('chat message',data);
    console.log(username)
    setFileUpload(1)
    setMsg("")
  }

  const onDrop = useCallback(acceptedFiles => {

    acceptedFiles.map(imgFile => {
      
      
      var reader = new FileReader();
    reader.onload = function(evt){

        var data ={};
        data.username = usernameRef.current.value;
        data.media = evt.target.result;
        data.fileName = imgFile.path;
        data.message = msg;
        data.room = roomNo;
        //data.fileName = data.name;
        socket.emit('chat message', data);

        console.log(data.username)
    };
    reader.readAsDataURL(imgFile);      

    });
  }, []);

  const onMessageImgSubmit = (imgFile) =>{
    
    var reader = new FileReader();
    reader.onload = function(evt){

        var data ={};
        data.username = username;
        data.media = evt.target.result;
        data.fileName = imgFile.path;
        data.message = msg;
        data.room = roomNo;
        //data.fileName = data.name;
        socket.emit('chat message', data);

        console.log(data.username)
    };
    reader.readAsDataURL(imgFile);
  }

  const onClickRoomChng = (value) =>{
    setRoom(value)
    socket.emit('join',roomNo)
  }

  const scrollToBottom = () => {
    messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
  }

  const renderChat = () => {
    
    let currUser = username
    let othUser =''
    const chatRec = chat
    if (chat.length!==0){
      return chatRec.map(({ username, message, media }, idx) => (
            
      <div className={username===currUser?"chatItem currUserItem":"chatItem"}>
        <p className={username===currUser?"chatBox currUser":"chatBox otherUser"} key={idx} ref={messagesEndRef}>
          <span>{message}</span>
          {
            media?
          <span><img src={media}/></span> : console.log("noImg")
          }
           
        </p>
      </div>
      ));
    }
    else{
      return(<div className="chatItem currUserItem">
        <p className="chatBox currUser" ref={messagesEndRef}>
        Welcome to Chatzee
        </p>
            </div>)
    }
    
  }


  

  
  return (
    <div className="App">
      <div className="App-content">
        <div className="App-header">
        <div>Chatzee</div>
        <div className={otherUser!==""? "userOnline":null}>{otherUser}</div>
        <input
          className="App-userName"
          name="nickname"
          placeholder="username"
          onChange={e => onUserChange(e)}
          value={username}
          ref={usernameRef} 
        />
      </div>
      <Dropzone onDrop={onDrop} accept={"image/*"} />
      <div className="App-read">
        
        {renderChat()}
      </div>
      <div className="App-userSection">
        <div className="App-userSubSec">
          <input
          className="App-userInput"
          name="msg"
          placeholder=" Type something"
          onChange={e => onTextChange(e)}
          onFocus={()=>setEmojiStatus(1)}
          value={msg}
        />
         
          <button className="App-userEmojiBtn" onFocus={()=>setEmojiStatus(0)} >&#128512;</button>
          <div className="fileinputs App-userEmojiBtn">
            
            {
            fileStatus===0? <input className={fileSelStatus} type="file" id="img" name="img" accept="image/*" onChange={e => onImgChange(e)}/> 
                          :
                          <input className={fileSelStatus} /> 
            }
            <button className="App-userFileBtn" onClick={()=>setFileUpload(0)} >&#x1f4ce;</button>
          </div>
         
          <div className={appEmojiStatus}>
          <Picker onEmojiClick={onEmojiClick} />
          </div>
          
        </div>     
        <button className="App-sendButton" onClick={onMessageSubmit}>&#x27A4;</button>     
       </div>
      </div>
    </div>
  );
}

export default App;