# README #

This is a chat application created using Socket.io and ReactJS. The application consists of two main files server and client. 
-Server : creates a server for the application using socketio and nodejs
-Client : creates a frontend chat aaplication interface to interact with the server

To run this application, guid terminal to server ( cd server ) and then type:

npm run dev

This command is able to start both server and client. Launches the application to your default browser.
Enter your username before chatting. The application is able to:

-send text messages
-send emoji
-send files(images)
-send files (drag and drop)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////